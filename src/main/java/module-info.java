module org.refcodes.archetype {
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.properties.ext.application;

	requires static org.refcodes.io;
	requires static org.refcodes.decoupling.ext.application;

	exports org.refcodes.archetype;
}
