// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.archetype;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import org.refcodes.data.Delimiter;
import org.refcodes.io.FileUtility;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.TomlProperties;
import org.refcodes.properties.TomlPropertiesBuilder;
import org.refcodes.runtime.Execution;

/**
 * The {@link C2Helper} provides means to manage launching and connecting to a
 * C2 server.
 */
public class C2Helper {

	private static final String DEFAULT_INSTANCE_ALIAS = "lock";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private RuntimeLogger LOGGER;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _instanceAlias;
	private Class<?> _resourceLocator;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new C2 helper.
	 *
	 * @param aInstanceAlias The alias identifying the invoking application's
	 *        instance.
	 * @param aResourceLocator The application's {@link Class} which to use when
	 *        loading resources (of the according module) by invoking
	 *        {@link Class#getResourceAsStream(String)}.
	 * @param isVerbose True to be more verbose.
	 * @param isForce the is force
	 * @param aLogger The application's logger.
	 * 
	 * @throws IOException thrown in I/O problems occurred building the
	 *         {@link C2Helper}.
	 */
	public C2Helper( String aInstanceAlias, Class<?> aResourceLocator, boolean isVerbose, boolean isForce, RuntimeLogger aLogger ) throws IOException {
		LOGGER = aLogger != null ? aLogger : RuntimeLoggerFactorySingleton.createRuntimeLogger();
		_resourceLocator = toResourceLocator( aResourceLocator );
		_instanceAlias = toInstanceAlias( aInstanceAlias );
		if ( isForce ) {
			deleteLockFile( isVerbose );
		}
	}

	private C2Helper( Builder builder ) throws IOException {
		this( builder.instanceAlias, builder.resourceLocator, builder.isVerbose, builder.isForce, builder.logger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Manually deletes the lockfile in case an orphaned lockfile is in the way.
	 * 
	 * @param isVerbose Be more verbose.
	 * 
	 * @throws IOException in case there were I/O problems reading the lock
	 *         file.
	 */
	public void deleteLockFile( boolean isVerbose ) throws IOException {
		final File theLockFile = FileUtility.openTempFile( getLockFileName() );
		if ( theLockFile.exists() && theLockFile.isFile() ) {
			if ( isVerbose ) {
				LOGGER.info( "Deleting C2-lockfile <" + theLockFile.getAbsolutePath() + "> ..." );
			}
			theLockFile.delete();
		}
	}

	/**
	 * Returns the instance alias assigned to this {@link C2Helper} instance.
	 * 
	 * @return the according instance alias.
	 */
	public String getInstanceAlias() {
		return _instanceAlias;
	}

	/**
	 * Gets the lock file name.
	 *
	 * @return the lock file name
	 */
	public String getLockFileName() {
		return _resourceLocator.getPackageName() + Delimiter.PACKAGE_HIERARCHY.getChar() + _instanceAlias;
	}

	/**
	 * Reads the lockfile and returns its content as a {@link Properties}
	 * instance. The properties in the lockfiles are expected to be in TOML
	 * notation (as of {@link TomlProperties}).
	 * 
	 * @param isVerbose Be more verbose.
	 * 
	 * @return The lockfile's {@link Properties}.
	 * 
	 * @throws IOException in case there were I/O problems reading the lock
	 *         file.
	 * @throws ParseException thrown in case parsing the lockfile's properties
	 *         failed.
	 */
	public Properties readLockFile( boolean isVerbose ) throws IOException, ParseException {
		final File theLockFile = FileUtility.openTempFile( getLockFileName() );
		if ( theLockFile.exists() && theLockFile.isFile() ) {
			if ( isVerbose ) {
				LOGGER.info( "Reading C2-lockfile <" + theLockFile.getAbsolutePath() + "> ..." );
			}
			return new TomlProperties( theLockFile );
		}
		return null;
	}

	/**
	 * Writes the lockfile with the content of the provided {@link Properties}
	 * instance. The properties in the lockfiles are being written in TOML
	 * notation (as of {@link TomlProperties}).
	 *
	 * @param aProperties The lockfile's {@link Properties}.
	 * @param isVerbose Be more verbose.
	 * 
	 * @throws IOException in case there were I/O problems reading the file.
	 */
	public void writeLockFile( Properties aProperties, boolean isVerbose ) throws IOException {
		final File theLockFile;
		theLockFile = FileUtility.createTempFile( getLockFileName(), true );
		if ( isVerbose ) {
			LOGGER.info( "Writing C2-lockfile <" + theLockFile.getAbsolutePath() + "> ..." );
		}
		new TomlPropertiesBuilder( aProperties ).saveTo( theLockFile );
	}

	private String toInstanceAlias( String aInstanceAlias ) {
		return aInstanceAlias != null ? aInstanceAlias : ( _resourceLocator != getClass() ? _resourceLocator.getSimpleName() : DEFAULT_INSTANCE_ALIAS );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private Class<?> toResourceLocator( Class<?> aResourceLocator ) {
		if ( aResourceLocator == null ) {
			aResourceLocator = Execution.getMainClass();
			if ( aResourceLocator == null ) {
				aResourceLocator = Execution.getCallerType( Builder.class );
				if ( aResourceLocator == null ) {
					aResourceLocator = Execution.getCallerType( getClass() );
					if ( aResourceLocator == null ) {
						aResourceLocator = getClass();
					}
				}
			}
		}
		return aResourceLocator;
	}

	/**
	 * Creates builder to build {@link C2Helper}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * AbstractBuilderCli to build {@link C2Helper} instances.
	 */
	public static final class Builder {

		private RuntimeLogger logger = null;
		private String instanceAlias = null;
		private Class<?> resourceLocator = null;
		private boolean isVerbose = true;
		private boolean isForce = false;

		private Builder() {}

		/**
		 * Creates a new {@link C2Helper} using the configured properties.
		 * 
		 * @return The accordingly configured {@link C2Helper}.
		 * 
		 * @throws IOException thrown in I/O problems occurred building the
		 *         {@link C2Helper}.
		 */
		public C2Helper build() throws IOException {
			return new C2Helper( this );
		}

		/**
		 * AbstractBuilderCli method setting the application's force mode.
		 * 
		 * @param isForce True in case to be more verbose.
		 * 
		 * @return This {@link AbstractCliBuilder} as of the builder pattern.
		 */
		public Builder withForce( boolean isForce ) {
			this.isForce = isForce;
			return this;
		}

		/**
		 * AbstractBuilderCli method providing the application's instance alias.
		 * 
		 * @param aInstanceAlias The application's alias identifying the
		 *        instance.
		 * 
		 * @return This {@link AbstractCliBuilder} as of the builder pattern.
		 */
		public Builder withInstanceAlias( String aInstanceAlias ) {
			instanceAlias = aInstanceAlias;
			return this;
		}

		/**
		 * AbstractBuilderCli method providing the application's
		 * {@link RuntimeLogger}.
		 * 
		 * @param aLogger The application's {@link RuntimeLogger}.
		 * 
		 * @return This {@link AbstractCliBuilder} as of the builder pattern.
		 */
		public Builder withLogger( RuntimeLogger aLogger ) {
			logger = aLogger;
			return this;
		}

		/**
		 * AbstractBuilderCli method providing the {@link Class} when doing a
		 * lookup of a (module specific) resource.
		 * 
		 * @param aResourceLocator The application's {@link Class} which to use
		 *        when loading resources (of the according module) by invoking
		 *        {@link Class#getResourceAsStream(String)}.
		 * 
		 * @return This {@link AbstractCliBuilder} as of the builder pattern.
		 */
		public Builder withResourceClass( Class<?> aResourceLocator ) {
			resourceLocator = aResourceLocator;
			return this;
		}

		/**
		 * AbstractBuilderCli method setting the application's verbose mode.
		 * 
		 * @param isVerbose True in case to be more verbose.
		 * 
		 * @return This {@link AbstractCliBuilder} as of the builder pattern.
		 */
		public Builder withVerbose( boolean isVerbose ) {
			this.isVerbose = isVerbose;
			return this;
		}
	}
}
