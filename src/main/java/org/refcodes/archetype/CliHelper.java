// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.archetype;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.refcodes.archetype.CliHelper.ArgsParseMessageMode;
import org.refcodes.cli.ArgsFilter;
import org.refcodes.cli.ArgsProcessorBuilder;
import org.refcodes.cli.ArgsProvidierBuilder;
import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Condition;
import org.refcodes.cli.ConfigOption;
import org.refcodes.cli.DebugFlag;
import org.refcodes.cli.Example;
import org.refcodes.cli.HelpFlag;
import org.refcodes.cli.InitFlag;
import org.refcodes.cli.Operand;
import org.refcodes.cli.Option;
import org.refcodes.cli.Optionable;
import org.refcodes.cli.QuietFlag;
import org.refcodes.cli.SyntaxMetrics;
import org.refcodes.cli.SyntaxNotation;
import org.refcodes.cli.SysInfoFlag;
import org.refcodes.cli.Term;
import org.refcodes.cli.VerboseFlag;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.ExitCode;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.mixin.DebugAccessor;
import org.refcodes.mixin.VerboseAccessor;
import org.refcodes.properties.DocumentMetricsImpl;
import org.refcodes.properties.ResourceLoaderBuilder;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.properties.ext.application.ApplicationPropertiesAccessor;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.Host;
import org.refcodes.textual.Font;
import org.refcodes.textual.TextBoxGrid;
import org.refcodes.textual.TextBoxStyle;

/**
 * The {@link CliHelper} provides means to support CLI command line argument
 * parsing.E.g. using the {@link CliHelper} you easily can create configuration
 * files ("--init" or "--init path/to/your/config") from a template (found in
 * "src/main/resources"), print a help text ("--help"), show system information
 * ("--sysinfo") for support issues, regard being more verbose ("--verbose") or
 * more quiet ("--quiet"), load a configuration from a specific location
 * ("--config path/to/your/config"), all given that your command line syntax
 * given uses the according predefined args syntax {@link Term} elements such as
 * {@link HelpFlag}, {@link VerboseFlag}, {@link QuietFlag}, {@link InitFlag},
 * {@link SysInfoFlag}, {@link DebugFlag} or {@link ConfigOption} (and the
 * like).
 * 
 * The {@link CliHelper} can either be invoked via its constructor or by using
 * the {@link AbstractCliBuilder} retrieved by invoking the {@link #builder()}
 * method (allowing you to configure the {@link CliHelper} in a more verbose
 * way).
 */
public class CliHelper implements CliArchetype {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	protected static final String DEFAULT_EMPTY_ARGS_MESSAGE = "Please provide command line arguments specifying your intend!";
	protected static final String DEFAULT_PASSWORD_PROMPT = "Please enter password and hit [ENTER] > ";
	protected ApplicationProperties _properties;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _name;
	private String _passwordPrompt = null;
	private String _emptyArgsMessage = null;
	private HelpFlag _helpFlag = null;
	private boolean _isDebug;
	private boolean _isVerbose = false;
	private Consumer<Integer> _shutDownHook = null;
	private RuntimeLogger _logger;
	private URL _url;
	private InputStream _inputStream;
	private File _file;
	private String _filePath;
	private ConfigLocator _configLocator;
	private Class<?> _resourceClass;

	/**
	 * Creates a new {@link CliHelper} with all the properties mainly regarded
	 * to the {@link ApplicationProperties} used under the hood (for ease of use
	 * please use the {@link AbstractCliBuilder} be calling the
	 * {@link #builder()} method).
	 *
	 * @param aArgs The command line arguments.
	 * @param aShortOptionPrefix The short options' prefix to be set.
	 * @param aLongOptionPrefix The long options' prefix to be set.
	 * @param aArgsSyntax The args syntax root node of your command line syntax.
	 * @param aSyntaxMetrics the syntax metrics
	 * @param aTitle The title of the application.
	 * @param aName The name of your application.
	 * @param aDescription The description to use when printing out the help.
	 * @param aCopyright The copyright being applied.
	 * @param aLicense The license for the application.
	 * @param aExamples The {@link Example} examples to use when printing out
	 *        the help.
	 * @param aUrl The {@link URL} from which to load the configuration.
	 * @param aInputStream The {@link InputStream} from which to load the
	 *        configuration.s
	 * @param aFile {@link File} from which to load the configuration.
	 * @param aFilePath The name to your default config file.
	 * @param aConfigLocator The {@link ConfigLocator} to use when locating the
	 *        configuration file (defaults to {@link ConfigLocator#DEFAULT}.
	 * @param aResourceClass The application's {@link Class} which to use when
	 *        loading resources (of the according module) by invoking
	 *        {@link Class#getResourceAsStream(String)}.
	 * @param aDelimiters The delimiters to be used when parsing specific
	 *        configuration file notations.
	 * @param isVerboseFallback The fallback verbose mode if the verbose mode
	 *        cannot be determined otherwise.
	 * @param aPasswordPrompt The prompt to use when requesting a password.
	 * @param aEmptyArgsMessage The message to show when args parsing failed and
	 *        the args have been empty.
	 * @param aBannerFont The font for the banner to use.
	 * @param aBannerFontPalette The ASCII color palette for the banner to use.
	 * @param aSeparatorLnChar The line separator char to be used.
	 * @param aTextBoxGrid The {@link TextBoxGrid} (or {@link TextBoxStyle}) to
	 *        be used when drawing the banner.
	 * @param aLineBreak The line break to use when printing to the terminal.
	 * @param aConsoleWidth The console width when printing to the terminal.
	 * @param aMaxConsoleWidth The max console width when printing to the
	 *        terminal.
	 * @param isEscCodesEnabled True in case the usage of Escape-Codes is
	 *        enabled.
	 * @param aBannerEscCode The banner Escape-Code to use when printing to the
	 *        terminal.
	 * @param aBannerBorderEscCode The banner border Escape-Code to use when
	 *        printing to the terminal.
	 * @param aArgumentEscCode The argument Escape-Code to use when printing to
	 *        the terminal.
	 * @param aCommandEscCode The command Escape-Code to use when printing to
	 *        the terminal.
	 * @param aOptionEscCode The option Escape-Code to use when printing to the
	 *        terminal.
	 * @param aDescriptionEscCode The description Escape-Code to use when
	 *        printing to the terminal.
	 * @param aLineSeparatorEscCode The line separator Escape-Code to use when
	 *        printing to the terminal.
	 * @param aResetEscCode The reset Escape-Code to use when printing to the
	 *        terminal.
	 * @param aShutdownHook When provided, then this hook is called instead of
	 *        {@link System#exit(int)} (it is up to the shutdown hook to
	 *        terminate the application in the end).
	 * @param aStandardOut The {@link PrintStream} to use for standard out.
	 * @param aErrorOut The {@link PrintStream} to use for error out.
	 * @param aArgsParseMessageMode The {@link ArgsParseMessageMode} to use when
	 *        displaying error messages as of failure when parsing command line
	 *        arguments.
	 * @param aLogger The application's logger.
	 */
	public CliHelper( String[] aArgs, Character aShortOptionPrefix, String aLongOptionPrefix, Term aArgsSyntax, SyntaxMetrics aSyntaxMetrics, String aTitle, String aName, String aDescription, String aCopyright, String aLicense, Collection<Example> aExamples, URL aUrl, InputStream aInputStream, File aFile, String aFilePath, ConfigLocator aConfigLocator, Class<?> aResourceClass, char[] aDelimiters, boolean isVerboseFallback, String aPasswordPrompt, String aEmptyArgsMessage, Font aBannerFont, char[] aBannerFontPalette, Character aSeparatorLnChar, TextBoxGrid aTextBoxGrid, String aLineBreak, Integer aConsoleWidth, Integer aMaxConsoleWidth, Boolean isEscCodesEnabled, String aBannerEscCode, String aBannerBorderEscCode, String aArgumentEscCode, String aCommandEscCode, String aOptionEscCode, String aDescriptionEscCode, String aLineSeparatorEscCode, String aResetEscCode, Consumer<Integer> aShutdownHook, PrintStream aStandardOut, PrintStream aErrorOut, ArgsParseMessageMode aArgsParseMessageMode, RuntimeLogger aLogger ) {
		aTitle = ( aTitle != null && aTitle.length() != 0 ) ? aTitle : "<" + aName.toUpperCase() + ">";
		_logger = aLogger != null ? aLogger : RuntimeLoggerFactorySingleton.createRuntimeLogger();
		_shutDownHook = aShutdownHook;
		_name = aName;
		_emptyArgsMessage = aEmptyArgsMessage != null ? aEmptyArgsMessage : DEFAULT_EMPTY_ARGS_MESSAGE;
		_passwordPrompt = aPasswordPrompt != null ? aPasswordPrompt : DEFAULT_PASSWORD_PROMPT;
		_url = aUrl;
		_inputStream = aInputStream;
		_file = aFile;
		_filePath = aFilePath;
		_configLocator = aConfigLocator;
		_resourceClass = toResourceLocator( aResourceClass );
		_properties = new ApplicationProperties( aArgsSyntax, new DocumentMetricsImpl( aDelimiters ) );
		_properties.setSyntaxMetrics( aSyntaxMetrics != null ? aSyntaxMetrics : SyntaxNotation.GNU_POSIX );
		_properties.setName( aName );
		_properties.setTitle( aTitle );
		_properties.setCopyright( aCopyright );
		_properties.setLicense( aLicense );
		_properties.setBannerFont( aBannerFont );
		_properties.setBannerFontPalette( aBannerFontPalette );
		_properties.setDescription( aDescription );
		_properties.setExamples( aExamples );
		if ( aConsoleWidth != null && aConsoleWidth != -1 ) {
			_properties.setConsoleWidth( aConsoleWidth );
		}
		if ( aMaxConsoleWidth != null && aMaxConsoleWidth != -1 ) {
			_properties.setMaxConsoleWidth( aMaxConsoleWidth );
		}
		if ( aTextBoxGrid != null ) {
			_properties.setTextBoxGrid( aTextBoxGrid );
		}
		if ( aLineBreak != null ) {
			_properties.setLineBreak( aLineBreak );
		}
		if ( isEscCodesEnabled != null ) {
			_properties.setEscapeCodesEnabled( isEscCodesEnabled );
		}
		if ( aBannerEscCode != null ) {
			_properties.setBannerEscapeCode( aBannerEscCode );
		}
		if ( aBannerBorderEscCode != null ) {
			_properties.setBannerBorderEscapeCode( aBannerBorderEscCode );
		}
		if ( aArgumentEscCode != null ) {
			_properties.setArgumentEscapeCode( aArgumentEscCode );
		}
		if ( aCommandEscCode != null ) {
			_properties.setCommandEscapeCode( aCommandEscCode );
		}
		if ( aOptionEscCode != null ) {
			_properties.setOptionEscapeCode( aOptionEscCode );
		}
		if ( aDescriptionEscCode != null ) {
			_properties.setDescriptionEscapeCode( aDescriptionEscCode );
		}
		if ( aResetEscCode != null ) {
			_properties.setResetEscapeCode( aResetEscCode );
		}
		if ( aShortOptionPrefix != null ) {
			_properties.setShortOptionPrefix( aShortOptionPrefix );
		}
		if ( aLongOptionPrefix != null ) {
			_properties.setLongOptionPrefix( aLongOptionPrefix );
		}
		if ( aLineSeparatorEscCode != null ) {
			_properties.setLineSeparatorEscapeCode( aLineSeparatorEscCode );
		}
		if ( aSeparatorLnChar != null ) {
			_properties.setSeparatorLnChar( aSeparatorLnChar );
		}
		if ( aStandardOut != null ) {
			_properties.setStandardOut( aStandardOut );
		}
		if ( aErrorOut != null ) {
			_properties.setErrorOut( aErrorOut );
		}

		try {
			_properties.evalArgs( aArgs );
		}
		catch ( ArgsSyntaxException e ) {
			printHelp();
			if ( aArgs == null || aArgs.length == 0 ) {
				_properties.errorLn( _emptyArgsMessage != null && _emptyArgsMessage.length() != 0 ? _emptyArgsMessage : DEFAULT_EMPTY_ARGS_MESSAGE );
			}
			else {
				String theMessage = e.getMessage();
				switch ( aArgsParseMessageMode ) {
				case SIMPLE:
					theMessage = e.getMessage();
				case VERBOSE:
					theMessage = e.toMessage();
				case HEURISTIC:
					theMessage = e.getCause() instanceof ArgsSyntaxException exc ? exc.toHeuristicMessage() : e.toHeuristicMessage();

				}
				_properties.errorLn( theMessage );
			}
			exit( ExitCode.MISUSE );
		}

		// Print help:
		if ( _properties.getBoolean( HelpFlag.ALIAS ) ) {
			printHelp();
			exit( ExitCode.SUCCESS );
		}
		VerboseFlag theVerboseFlag = null;
		QuietFlag theQuietFlag = null;
		DebugFlag theDebugFlag = null;
		InitFlag theInitFlag = null;
		if ( aArgsSyntax instanceof Condition theCondition ) {
			theVerboseFlag = theCondition.toOperand( VerboseFlag.class );
			theQuietFlag = theCondition.toOperand( QuietFlag.class );
			theDebugFlag = theCondition.toOperand( DebugFlag.class );
			theInitFlag = theCondition.toOperand( InitFlag.class );
			_helpFlag = theCondition.toOperand( HelpFlag.class );
		}
		_isVerbose = isVerbose( theVerboseFlag, theQuietFlag, isVerboseFallback );
		_isDebug = theDebugFlag != null ? theDebugFlag.isEnabled() : false;

		if ( _isVerbose ) {
			printBanner();
		}
		try {
			// Config path argument:
			if ( theInitFlag != null && theInitFlag.isEnabled() ) {
				// Create config:
				createConfigFile();
				exit( ExitCode.SUCCESS );
			}
			else {
				// Load config:
				try {
					loadConfigFile();
				}
				catch ( Exception e ) {
					if ( _filePath != null && _filePath.length() != 0 ) {
						throw e;
					}
				}
				_isVerbose = isVerbose( theVerboseFlag, theQuietFlag, isVerboseFallback ); // After ARGS evaluation AND loading properties
				try {
					_isDebug = _properties.getBoolean( theDebugFlag != null ? theDebugFlag.getAlias() : DebugFlag.ALIAS ); // After ARGS evaluation AND loading properties
				}
				catch ( Exception ignore ) {}
			}
		}
		catch ( Exception e ) {
			if ( _isDebug ) {
				if ( _isVerbose ) {
					_logger.error( Trap.asMessage( e ), e );
				}
				else {
					_properties.errorLn( Trap.asMessage( e ) );
					e.printStackTrace();
				}
			}
			else {
				if ( _isVerbose ) {
					_logger.error( Trap.asMessage( e ) );
				}
				else {
					_properties.errorLn( Trap.asMessage( e ) );
				}
			}
			exit( e.hashCode() % 0xFF );
		}

		// Print system info:
		if ( _properties.getBoolean( SysInfoFlag.ALIAS ) ) {
			printSysInfo();
			exit( ExitCode.SUCCESS );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor used exclusively by the {@link AbstractCliBuilder} as of the
	 * {@link #builder()} method.
	 * 
	 * @param aBuilder The {@link AbstractCliBuilder} used to configure the
	 *        {@link CliHelper} instance.
	 */
	protected CliHelper( AbstractCliBuilder<?, ?> aBuilder ) {
		this( aBuilder.args, aBuilder.shortOptionPrefix, aBuilder.longOptionPrefix, aBuilder.argsSyntax, aBuilder.syntaxMetrics, aBuilder.title, aBuilder.name, aBuilder.description, aBuilder.copyright, aBuilder.license, aBuilder.examples, aBuilder.url, aBuilder.inputStream, aBuilder.file, aBuilder.filePath, aBuilder.configLocator, aBuilder.resourceClass, aBuilder.delimiters, aBuilder.verboseFallback, aBuilder.passwordPrompt, aBuilder.emptyArgsMessage, aBuilder.bannerFont, aBuilder.bannerFontPalette, aBuilder.separatorLnChar, aBuilder.textBoxGrid, aBuilder.lineBreak, aBuilder.consoleWidth, aBuilder.maxConsoleWidth, aBuilder.escCodesEnabled, aBuilder.bannerEscCode, aBuilder.bannerBorderEscCode, aBuilder.argumentEscCode, aBuilder.commandEscCode, aBuilder.optionEscCode, aBuilder.descriptionEscCode, aBuilder.lineSeparatorEscCode, aBuilder.resetEscCode, aBuilder.shutDownHook, aBuilder.standardOut, aBuilder.errorOut, aBuilder.argsParseMessageMode, aBuilder.logger );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createResourceFile( String aResourcePath, File aDestinationFile ) throws IOException {
		final String theResourceName = toResourceName( aResourcePath );
		final File theFile = aDestinationFile != null ? aDestinationFile : new File( theResourceName );
		if ( _isVerbose ) {
			_logger.info( "Creating file at <" + theFile.getAbsolutePath() + "> ..." );
		}
		try ( FileOutputStream theOutputStream = new FileOutputStream( theFile ); InputStream theInputStream = _resourceClass != null ? _resourceClass.getResourceAsStream( "/" + aResourcePath ) : CliHelper.class.getResourceAsStream( "/" + aResourcePath ) ) {
			theInputStream.transferTo( theOutputStream );
			theOutputStream.flush();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createResourceFile( String aResourcePath, String aDestinationPath ) throws IOException {
		final String theResourceName = toResourceName( aResourcePath );
		final String theDestinationPath = ( aDestinationPath != null && aDestinationPath.length() != 0 ) ? aDestinationPath : theResourceName;
		final File theFile = new File( theDestinationPath );
		if ( _isVerbose ) {
			_logger.info( "Creating file at <" + theFile.getAbsolutePath() + "> ..." );
		}
		try ( FileOutputStream theOutputStream = new FileOutputStream( theFile ); InputStream theInputStream = _resourceClass != null ? _resourceClass.getResourceAsStream( "/" + aResourcePath ) : CliHelper.class.getResourceAsStream( "/" + aResourcePath ) ) {
			theInputStream.transferTo( theOutputStream );
			theOutputStream.flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exit( ExitCode aExitCode ) {
		exit( aExitCode.getStatusCode() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exit( int aStatusCode ) {
		if ( _shutDownHook != null ) {
			_shutDownHook.accept( aStatusCode );
		}
		else {
			System.exit( aStatusCode );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exitOnException( Throwable e ) {
		printException( e );
		exit( e.hashCode() % 0xFF );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ApplicationProperties getApplicationProperties() {
		return _properties;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDebug() {
		return _isDebug;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVerbose() {
		return _isVerbose;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printException( Throwable e ) {
		if ( !_isVerbose ) {
			_logger.printTail();
		}
		final String theMessage = Trap.asMessage( e );
		if ( _isDebug ) {
			if ( _isVerbose ) {
				_logger.error( theMessage, e );
			}
			else {
				System.out.println( theMessage );
				e.printStackTrace();
			}

		}
		else {
			if ( _isVerbose ) {
				_logger.error( theMessage );
			}
			else {
				System.out.println( theMessage );
			}
		}

		if ( _helpFlag != null ) {
			if ( _isVerbose ) {
				_logger.info( "Call '" + _name + " " + ( _properties != null ? _properties.getLongOptionPrefix() : SyntaxNotation.DEFAULT.getLongOptionPrefix() ) + _helpFlag.getLongOption() + "' for help!" );
			}
			else {
				System.out.println( "Call '" + _name + " " + ( _properties != null ? _properties.getLongOptionPrefix() : SyntaxNotation.DEFAULT.getLongOptionPrefix() ) + _helpFlag.getLongOption() + "' for help!" );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSysInfo() {
		if ( _isVerbose ) {
			_logger.printSeparator();
			for ( String eKey : _properties.sortedKeys() ) {
				_logger.info( eKey + " = " + _properties.get( eKey ) );
			}
			_logger.printSeparator();
			final Map<String, String> theSysinfo = Host.toSystemInfo();
			final List<String> theKeys = new ArrayList<>( theSysinfo.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				_logger.info( eKey + " = " + theSysinfo.get( eKey ) );
			}
			_logger.printSeparator();
		}
		else {
			System.out.println( Host.toPrettySystemInfo() );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] readPassword() {
		return readPassword( _passwordPrompt );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] readPassword( String aPrompt ) {
		if ( _isVerbose ) {
			_logger.printTail();
		}
		System.console().printf( aPrompt );
		final char[] theReadPassword = System.console().readPassword();
		if ( theReadPassword == null || theReadPassword.length == 0 ) {
			exit( ExitCode.CONTROL_C );
		}
		return theReadPassword;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toOptions( Option<?> aOption ) {
		return _properties.toOptions( aOption );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Prints the application's banner to the console.
	 */
	protected void printBanner() {
		_properties.printBanner();
	}

	/**
	 * Prints the application's help to the console.
	 */
	protected void printHelp() {
		_properties.printHelp();
	}

	private void createConfigFile() throws IOException {
		String theConfigPath = _properties.getOr( ConfigOption.ALIAS, _filePath );
		theConfigPath = ( theConfigPath != null && theConfigPath.length() != 0 ) ? theConfigPath : _filePath;
		final File theFile = new File( theConfigPath );
		if ( _isVerbose ) {
			_logger.info( "Initializing file at <" + theFile.getAbsolutePath() + "> ..." );
		}
		try ( FileOutputStream theOutputStream = new FileOutputStream( theFile ); InputStream theInputStream = _resourceClass != null ? _resourceClass.getResourceAsStream( "/" + _filePath ) : CliHelper.class.getResourceAsStream( "/" + _filePath ) ) {
			theInputStream.transferTo( theOutputStream );
			theOutputStream.flush();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private boolean isVerbose( VerboseFlag aVerboseFlag, QuietFlag aQuietFlag, boolean isVerboseFallback ) {
		if ( aVerboseFlag == null && aQuietFlag == null && !_properties.containsKey( QuietFlag.ALIAS ) && !_properties.containsKey( VerboseFlag.ALIAS ) ) {
			return isVerboseFallback;
		}
		if ( aVerboseFlag != null && aVerboseFlag.hasValue() ) {
			return aVerboseFlag.isEnabled();
		}
		else if ( aQuietFlag != null && aQuietFlag.hasValue() ) {
			return !aQuietFlag.isEnabled();
		}
		else {
			final boolean isQuiet = _properties.containsKey( QuietFlag.ALIAS ) && !_properties.getBoolean( QuietFlag.ALIAS );
			final boolean isVerbose = _properties.containsKey( VerboseFlag.ALIAS ) && _properties.getBoolean( VerboseFlag.ALIAS );
			return ( ( aQuietFlag != null && !isQuiet ) || ( aVerboseFlag != null && isVerbose ) );
		}
	}

	private void loadConfigFile() throws ParseException, IOException {
		if ( _url != null ) {
			_properties.withUrl( _url );
			if ( _isVerbose ) {
				_logger.info( "Loaded config from <" + _url.toString() + "> ..." );
				_logger.printSeparator();
			}
		}
		else if ( _inputStream != null ) {
			_properties.withInputStream( _inputStream );
			if ( _isVerbose ) {
				_logger.info( "Loaded config _inputStream <" + _inputStream.toString() + "> ..." );
				_logger.printSeparator();
			}
		}
		else if ( _file != null ) {
			_properties.withFile( _file );
			if ( _isVerbose ) {
				_logger.info( "Loaded config from <" + _file.toString() + "> ..." );
				_logger.printSeparator();
			}
		}
		else {
			final String aConfigPath = _properties.getOr( ConfigOption.ALIAS, _filePath );
			if ( aConfigPath != null && aConfigPath.length() != 0 ) {
				_properties.withResourceClass( _resourceClass, aConfigPath, _configLocator );
				if ( _isVerbose ) {
					_logger.info( "Loaded config from <" + aConfigPath + "> ..." );
					_logger.printSeparator();
				}
			}
			else if ( _filePath != null && _filePath.length() != 0 ) {
				try {
					_properties.withResourceClass( _resourceClass, _filePath );
				}
				catch ( IOException ignore ) {
					if ( _isVerbose ) {
						_logger.warn( "Config file with name <" + _filePath + "> not found (skipping) as of: " + Trap.asMessage( ignore ) );
					}
				}
			}
		}
	}

	private Class<?> toResourceLocator( Class<?> aResourceLocator ) {
		if ( aResourceLocator == null ) {
			aResourceLocator = Execution.getMainClass();
			if ( aResourceLocator == null ) {
				aResourceLocator = Execution.getCallerType( AbstractCliBuilder.class );
				if ( aResourceLocator == null ) {
					aResourceLocator = Execution.getCallerType( getClass() );
					if ( aResourceLocator == null ) {
						aResourceLocator = getClass();
					}
				}
			}
		}
		return aResourceLocator;
	}

	private String toResourceName( String aResourcePath ) {
		String theResourceName = aResourcePath;
		int index = theResourceName.indexOf( File.pathSeparator );
		while ( index != -1 && index != theResourceName.length() - 1 ) {
			theResourceName = theResourceName.substring( index + 1 );
			index = theResourceName.indexOf( File.pathSeparator );
		}
		return theResourceName;
	}

	/**
	 * Creates builder to build {@link CliHelper}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * The {@link ArgsParseMessageMode} determines the details level for
	 * messages printed out upon parse failure of the command line arguments.
	 */
	public enum ArgsParseMessageMode {

		/**
		 * Print just the message of the thrown exception, do not try to be more
		 * verbose or heuristic.
		 */
		SIMPLE,

		/**
		 * Create a summary which point out (heuristically tries to point out)
		 * the most probable case of failing to provide the correct command line
		 * arguments (as of the command line syntax).
		 */
		HEURISTIC,

		/**
		 * Show all possible cases responsible for failing to parse the command
		 * line arguments (as of the command line syntax).
		 */
		VERBOSE
	}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@link Builder} to build {@link CliHelper}.
	 */
	public static class Builder extends AbstractCliBuilder<Builder, CliHelper> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public CliHelper build() {
			return new CliHelper( this );
		}
	}
}

/**
 * Generic {@link AbstractCliBuilder} to build a {@link CliHelper} and the like.
 *
 * @param <B> the generic of the {@link AbstractCliBuilder} used when chaining
 *        methods.
 * @param <R> the generic type of the return type of the
 *        {@link AbstractCliBuilder#build()} method.
 */
abstract class AbstractCliBuilder<B extends AbstractCliBuilder<B, R>, R> implements ArgsProcessorBuilder<AbstractCliBuilder<B, R>>, ResourceLoaderBuilder<AbstractCliBuilder<B, R>>, ArgsProvidierBuilder<AbstractCliBuilder<B, R>> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Character shortOptionPrefix;
	protected String longOptionPrefix;
	protected String name = null;
	protected String passwordPrompt = null;
	protected String emptyArgsMessage = null;
	protected String[] args = null;
	protected Term argsSyntax = null;
	protected Font bannerFont = null;
	protected char[] bannerFontPalette = null;
	protected String copyright = null;
	protected String description = null;
	protected Collection<Example> examples = new ArrayList<>();
	protected String license = null;
	protected RuntimeLogger logger = null;
	protected String title = null;
	protected boolean verboseFallback = false;
	protected Consumer<Integer> shutDownHook = null;
	protected Integer consoleWidth = null;
	protected Integer maxConsoleWidth = null;
	protected Boolean escCodesEnabled = null;
	protected TextBoxGrid textBoxGrid = null;
	protected String lineBreak = null;
	protected String descriptionEscCode = null;
	protected String commandEscCode = null;
	protected String bannerEscCode = null;
	protected String bannerBorderEscCode = null;
	protected String resetEscCode = null;
	protected String optionEscCode = null;
	protected String argumentEscCode = null;
	protected SyntaxMetrics syntaxMetrics = null;
	protected File file = null;
	protected ConfigLocator configLocator = ConfigLocator.DEFAULT;
	protected char[] delimiters = null;
	protected InputStream inputStream = null;
	protected Class<?> resourceClass = null;
	protected String filePath = null;
	protected URL url = null;
	protected String lineSeparatorEscCode;
	protected Character separatorLnChar;
	protected ArgsParseMessageMode argsParseMessageMode = ArgsParseMessageMode.HEURISTIC;
	protected PrintStream standardOut;
	protected PrintStream errorOut;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new builder.
	 */
	protected AbstractCliBuilder() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new instance to be constructed by this builder.
	 * 
	 * @return The accordingly configured new instance.
	 */
	public abstract R build();

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withAddExample( Example aExamples ) {
		examples.add( aExamples );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withAddExample( String aDescription, Operand<?>... aOperands ) {
		withAddExample( new Example( aDescription, aOperands ) );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withArgs( List<String> aArgs ) {
		args = aArgs.toArray( new String[aArgs.size()] );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withArgs( List<String> aArgs, ArgsFilter aArgsFilter ) {
		return withArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withArgs( List<String> aArgs, Pattern aFilterExp ) {
		return withArgs( ArgsFilter.toFiltered( aArgs.toArray( new String[aArgs.size()] ), aFilterExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withArgs( String[] aArgs ) {
		args = aArgs;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withArgs( String[] aArgs, ArgsFilter aArgsFilter ) {
		return withArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withArgs( String[] aArgs, Pattern aFilterExp ) {
		return withArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}

	/**
	 * Builder method providing the {@link ArgsParseMessageMode} used to defined
	 * the detail level when print out argument parsing errors.
	 * 
	 * @param aArgsParseMessageMode The mode defining the detail level of error
	 *        messages while parsing command line arguments.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withArgsParseMessageMode( ArgsParseMessageMode aArgsParseMessageMode ) {
		argsParseMessageMode = aArgsParseMessageMode;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withArgsSyntax( Term aArgsSyntax ) {
		argsSyntax = aArgsSyntax;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withArgumentEscapeCode( String aArgumentEscCode ) {
		argumentEscCode = aArgumentEscCode;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withBannerBorderEscapeCode( String aBannerBorderEscCode ) {
		bannerBorderEscCode = aBannerBorderEscCode;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withBannerEscapeCode( String aBannerEscCode ) {
		bannerEscCode = aBannerEscCode;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withBannerFont( Font aBannerFont ) {
		bannerFont = aBannerFont;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withBannerFontPalette( AsciiColorPalette aBannerFontPalette ) {
		bannerFontPalette = aBannerFontPalette.getPalette();
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withBannerFontPalette( char[] aBannerFontPalette ) {
		bannerFontPalette = aBannerFontPalette;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withCommandEscapeCode( String aCommandEscCode ) {
		commandEscCode = aCommandEscCode;
		return (B) this;
	}

	/**
	 * The {@link ConfigLocator} to use when locating the configuration file
	 * (defaults to {@link ConfigLocator#DEFAULT}.
	 * 
	 * @param aConfigLocator The {@link ConfigLocator} to use when locating the
	 *        configuration file (defaults to {@link ConfigLocator#DEFAULT}.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withConfigLocator( ConfigLocator aConfigLocator ) {
		configLocator = aConfigLocator;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withConsoleWidth( int aConsoleWidth ) {
		consoleWidth = aConsoleWidth;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withCopyright( String aCopyright ) {
		copyright = aCopyright;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withDescription( String aDescription ) {
		description = aDescription;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withDescriptionEscapeCode( String aDescriptionEscCode ) {
		descriptionEscCode = aDescriptionEscCode;
		return (B) this;
	}

	/**
	 * Builder method providing the application's default empty args message.
	 * 
	 * @param aEmptyArgsMessage he message to show when args parsing failed and
	 *        the args have been empty.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withEmptyArgsMessage( String aEmptyArgsMessage ) {
		emptyArgsMessage = aEmptyArgsMessage;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withErrorOut( PrintStream aErrorOut ) {
		errorOut = aErrorOut;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withEscapeCodesEnabled( boolean isEscCodesEnabled ) {
		escCodesEnabled = isEscCodesEnabled;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withExamples( Collection<Example> aExamples ) {
		examples.addAll( aExamples );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withExamples( Example[] aExamples ) {
		examples = Arrays.asList( aExamples );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withFile( File aFile ) throws IOException, ParseException {
		return withFile( aFile, ConfigLocator.ALL );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withFile( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		file = aFile;
		configLocator = aConfigLocator;
		inputStream = null;
		resourceClass = null;
		filePath = null;
		url = null;
		return (B) this;
	}

	/**
	 * The name to your default config file.
	 * 
	 * @param aFilePath The name to your default config file.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withFilePath( String aFilePath ) {
		filePath = aFilePath;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withInputStream( InputStream aInputStream ) throws IOException, ParseException {
		inputStream = aInputStream;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withLicense( String aLicense ) {
		license = aLicense;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withLineBreak( String aLineBreak ) {
		lineBreak = aLineBreak;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withLineSeparatorEscapeCode( String aLineSeparatorEscCode ) {
		lineSeparatorEscCode = aLineSeparatorEscCode;
		return (B) this;
	}

	/**
	 * Builder method providing the application's {@link RuntimeLogger}.
	 * 
	 * @param aLogger The application's {@link RuntimeLogger}.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withLogger( RuntimeLogger aLogger ) {
		logger = aLogger;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withLongOptionPrefix( String aLongOptionPrefix ) {
		longOptionPrefix = aLongOptionPrefix;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withMaxConsoleWidth( int aMaxConsoleWidth ) {
		maxConsoleWidth = aMaxConsoleWidth;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withName( String aName ) {
		name = aName;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withOptionEscapeCode( String aOptionEscCode ) {
		optionEscCode = aOptionEscCode;
		return (B) this;
	}

	/**
	 * Builder method providing the application's default password prompt.
	 * 
	 * @param aPasswordPrompt The application's default password prompt.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withPasswordPrompt( String aPasswordPrompt ) {
		passwordPrompt = aPasswordPrompt;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withResetEscapeCode( String aResetEscCode ) {
		resetEscCode = aResetEscCode;
		return (B) this;
	}

	/**
	 * Builder method providing the application's {@link Class} which to use
	 * when loading resources.
	 * 
	 * @param aResourceClass The application's {@link Class} which to use when
	 *        loading resources (of the according module) by invoking
	 *        {@link Class#getResourceAsStream(String)}.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withResourceClass( Class<?> aResourceClass ) {
		resourceClass = aResourceClass;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withResourceClass( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
		return withResourceClass( aResourceClass, aFilePath, ConfigLocator.ALL );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withResourceClass( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		file = null;
		configLocator = aConfigLocator;
		inputStream = null;
		resourceClass = aResourceClass;
		filePath = aFilePath;
		url = null;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withResourceClass( String aFilePath ) throws IOException, ParseException {
		return withResourceClass( Execution.getMainClass(), aFilePath, ConfigLocator.ALL );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withResourceClass( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		return withResourceClass( null, aFilePath, aConfigLocator );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withSeparatorLnChar( char aSeparatorLnChar ) {
		separatorLnChar = aSeparatorLnChar;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withShortOptionPrefix( Character aShortOptionPrefix ) {
		shortOptionPrefix = aShortOptionPrefix;
		return (B) this;
	}

	/**
	 * Builder method providing an (optional) shutdown hook: When provided, then
	 * this hook is called instead of {@link System#exit(int)} (it is up to the
	 * shutdown hook to terminate the application in the end).
	 * 
	 * @param aShutDownHook The (optional) shutdown hook.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withShutDownHook( Consumer<Integer> aShutDownHook ) {
		shutDownHook = aShutDownHook;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withStandardOut( PrintStream aStandardOut ) {
		standardOut = aStandardOut;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
		syntaxMetrics = aSyntaxMetrics;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B withSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
		return withSyntaxMetrics( (SyntaxMetrics) aSyntaxNotation );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		textBoxGrid = aTextBoxGrid;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
		textBoxGrid = aTextBoxStyle;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public B withTitle( String aTitle ) {
		title = aTitle;
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withUrl( URL aUrl ) throws IOException, ParseException {
		file = null;
		configLocator = null;
		inputStream = null;
		resourceClass = null;
		filePath = null;
		url = aUrl;
		return (B) this;
	}

	/**
	 * Builder method providing the application's verbose fallback property. In
	 * case no {@link VerboseFlag} neither {@link QuietFlag} parsers are present
	 * and no "verbose" neither "quiet" value are found in the properties, then
	 * this value is used as a fallback, either to be "more verbose" (true) or
	 * "more quiet" (false).
	 * 
	 * @param aVerboseFallback The fallback verbose mode if the verbose mode
	 *        cannot be determined otherwise.
	 * 
	 * @return This {@link AbstractCliBuilder} as of the builder pattern.
	 */
	@SuppressWarnings("unchecked")
	public B withVerboseFallback( boolean aVerboseFallback ) {
		verboseFallback = aVerboseFallback;
		return (B) this;
	}
}

/**
 * Base functionality for any derived archetype.
 */
interface CliArchetype extends VerboseAccessor, DebugAccessor, Optionable, ApplicationPropertiesAccessor {

	/**
	 * Copies a resource file (stored in the fat JAR) to the given destination
	 * on the file system or (if the destination is not provided) to a file with
	 * the name of the resource to the file system.
	 * 
	 * @param aResourcePath The resource to copy from the fat JAR to the file
	 *        system.
	 * @param aDestinationFile The destination where to copy to or null if to
	 *        use the name of the resource.
	 * 
	 * @throws IOException thrown in case an I/O related problem occurred during
	 *         the copy operation.
	 */
	void createResourceFile( String aResourcePath, File aDestinationFile ) throws IOException;

	/**
	 * Copies a resource file (stored in the fat JAR) to the given destination
	 * on the file system or (if the destination is not provided) to a file with
	 * the name of the resource to the file system.
	 * 
	 * @param aResourcePath The resource to copy from the fat JAR to the file
	 *        system.
	 * @param aDestinationPath The destination where to copy to or null if to
	 *        use the name of the resource.
	 * 
	 * @throws IOException thrown in case an I/O related problem occurred during
	 *         the copy operation.
	 */
	void createResourceFile( String aResourcePath, String aDestinationPath ) throws IOException;

	/**
	 * Convenience method for exiting the application. Delegates to the
	 * {@link #exit(int)} method.
	 * 
	 * @param aExitCode The exit code {@link ExitCode#getStatusCode()} to be
	 *        passed to {@link #exit(int)}.
	 */
	void exit( ExitCode aExitCode );

	/**
	 * Hook method for exiting the application. By default,
	 * {@link System#exit(int)} is called, though in a smart phone app, other
	 * exit strategies might be required, which can be taken care of by
	 * overwriting this method.
	 * 
	 * @param aStatusCode The exit code to be passed.
	 */
	void exit( int aStatusCode );

	/**
	 * Exits upon a given exception by printing the exception according to any
	 * detected {@link VerboseFlag} as well as {@link DebugFlag} and the
	 * retrieved {@link ApplicationProperties} (see
	 * {@link #printException(Exception)}) and then exiting with an exit code
	 * derived from the exception type.
	 * 
	 * @param e The {@link Throwable} to gracefully print and from which to
	 *        derive the exit code.
	 */
	void exitOnException( Throwable e );

	/**
	 * Returns the {@link ApplicationProperties} as of the provided CLI
	 * configuration.
	 * 
	 * @return The according {@link ApplicationProperties} instance.
	 */
	@Override
	ApplicationProperties getApplicationProperties();

	/**
	 * Determines the verbose mode by evaluating the {@link VerboseFlag#ALIAS}
	 * and the {@link QuietFlag#ALIAS} properties after having constructed the
	 * {@link ApplicationProperties} from the command line arguments and the
	 * configuration file (e.g. the "quiet" property is false or the "verbose"
	 * property is true). Also takes into account a potentially existing
	 * {@link QuietFlag} or {@link VerboseFlag} given being provided by your
	 * command line syntax.
	 * 
	 * @return True in case to be more verbose.
	 */
	@Override
	boolean isVerbose();

	/**
	 * Prints the exception according to any detected {@link VerboseFlag} as
	 * well as {@link DebugFlag} and the retrieved
	 * {@link ApplicationProperties}.
	 * 
	 * @param e The {@link Throwable} to gracefully print.
	 */
	void printException( Throwable e );

	/**
	 * Prints system information helpful for debugging cases.
	 */
	void printSysInfo();

	/**
	 * Reads a password from the console using the configured (or the default)
	 * password prompt taking care of the verbose mode table's tail to be
	 * printed (in case necessary).
	 * 
	 * @return The char array representing the password. The array can be
	 *         cleaned (overridden char by char) afterwards to remove any hints
	 *         in memory
	 */
	char[] readPassword();

	/**
	 * Reads a password from the console taking care of the verbose mode table's
	 * tail to be printed (in case necessary).
	 * 
	 * @param aPrompt The prompt to print when reading the password.
	 * 
	 * @return The char array representing the password. The array can be
	 *         cleaned (overridden char by char) afterwards to remove any hints
	 *         in memory
	 */
	char[] readPassword( String aPrompt );

	/**
	 * {@inheritDoc}
	 */
	@Override
	String[] toOptions( Option<?> aOption );
}
