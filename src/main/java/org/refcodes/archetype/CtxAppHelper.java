// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.archetype;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.refcodes.archetype.CliHelper.ArgsParseMessageMode;
import org.refcodes.cli.ArgsFilter;
import org.refcodes.cli.Example;
import org.refcodes.cli.Operand;
import org.refcodes.cli.SyntaxMetrics;
import org.refcodes.cli.SyntaxNotation;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.Font;
import org.refcodes.textual.TextBoxGrid;
import org.refcodes.textual.TextBoxStyle;

/**
 * The {@link CtxAppHelper} is a stripped down {@link CtxHelper} intended to be
 * used by GUI applications which must(!) avoid (to link to)
 * <code>java.awt</code> which might not be available on a target platform such
 * as Android. Background: For mere nerd purposes, the {@link CliHelper} used by
 * the {@link CtxHelper} comes with a full blown ASCII-Art banner which uses an
 * AWT graphics context to convert text fonts or graphics to ASCII-Art. The AWT
 * graphics context is not available on Android and causes applications built
 * with the GraalVM and Gluon's substrate to crash as of an unavailable
 * "fontmanager symbol".
 */
public class CtxAppHelper extends CtxHelper {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CtxAppHelper( String[] aArgs, Character aShortOptionPrefix, String aLongOptionPrefix, Term aArgsSyntax, SyntaxMetrics aSyntaxMetrics, String aTitle, String aName, String aDescription, String aCopyright, String aLicense, Collection<Example> aExamples, URL aUrl, InputStream aInputStream, File aFile, String aFilePath, ConfigLocator aConfigLocator, Class<?> aResourceClass, char[] aDelimiters, boolean isVerboseFallback, String aPasswordPrompt, String aEmptyPasswordMessage, Font aBannerFont, char[] aBannerFontPalette, Character aSeparatorLnChar, TextBoxGrid aTextBoxGrid, String aLineBreak, Integer aConsoleWidth, Integer aMaxConsoleWidth, Boolean aIsEscCodesEnabled, String aBannerEscCode, String aBannerBorderEscCode, String aArgumentEscCode, String aCommandEscCode, String aOptionEscCode, String aDescriptionEscCode, String aLineSeparatorEscCode, String aResetEscCode, Consumer<Integer> aShutdownHook, PrintStream aStandardOut, PrintStream aErrorOut, ArgsParseMessageMode aArgsParseMessageMode, RuntimeLogger aLogger ) {
		super( aArgs, aShortOptionPrefix, aLongOptionPrefix, aArgsSyntax, aSyntaxMetrics, aTitle, aName, aDescription, aCopyright, aLicense, aExamples, aUrl, aInputStream, aFile, aFilePath, aConfigLocator, aResourceClass, aDelimiters, isVerboseFallback, aPasswordPrompt, aEmptyPasswordMessage, aBannerFont, aBannerFontPalette, aSeparatorLnChar, aTextBoxGrid, aLineBreak, aConsoleWidth, aMaxConsoleWidth, aIsEscCodesEnabled, aBannerEscCode, aBannerBorderEscCode, aArgumentEscCode, aCommandEscCode, aOptionEscCode, aDescriptionEscCode, aLineSeparatorEscCode, aResetEscCode, aShutdownHook, aStandardOut, aErrorOut, aArgsParseMessageMode, aLogger );
	}

	/**
	 * {@inheritDoc}
	 */
	protected CtxAppHelper( AbstractCliBuilder<?, ?> aBuilder ) {
		super( aBuilder.args, aBuilder.shortOptionPrefix, aBuilder.longOptionPrefix, aBuilder.argsSyntax, aBuilder.syntaxMetrics, aBuilder.title, aBuilder.name, aBuilder.description, aBuilder.copyright, aBuilder.license, aBuilder.examples, aBuilder.url, aBuilder.inputStream, aBuilder.file, aBuilder.filePath, aBuilder.configLocator, aBuilder.resourceClass, aBuilder.delimiters, aBuilder.verboseFallback, aBuilder.passwordPrompt, aBuilder.emptyArgsMessage, aBuilder.bannerFont, aBuilder.bannerFontPalette, aBuilder.separatorLnChar, aBuilder.textBoxGrid, aBuilder.lineBreak, aBuilder.consoleWidth, aBuilder.maxConsoleWidth, aBuilder.escCodesEnabled, aBuilder.bannerEscCode, aBuilder.bannerBorderEscCode, aBuilder.argumentEscCode, aBuilder.commandEscCode, aBuilder.optionEscCode, aBuilder.descriptionEscCode, aBuilder.lineSeparatorEscCode, aBuilder.resetEscCode, aBuilder.shutDownHook, aBuilder.standardOut, aBuilder.errorOut, aBuilder.argsParseMessageMode, aBuilder.logger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates builder to build {@link CtxAppHelper}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CliHelper toCliHelper( String[] aArgs, Character aShortOptionPrefix, String aLongOptionPrefix, Term aArgsSyntax, SyntaxMetrics aSyntaxMetrics, String aTitle, String aName, String aDescription, String aCopyright, String aLicense, Collection<Example> aExamples, URL aUrl, InputStream aInputStream, File aFile, String aFilePath, ConfigLocator aConfigLocator, Class<?> aResourceClass, char[] aDelimiters, String aPasswordPrompt, String aEmptyArgsMessage, Font aBannerFont, char[] aBannerFontPalette, Character aSeparatorLnChar, TextBoxGrid aTextBoxGrid, String aLineBreak, Integer aConsoleWidth, Integer aMaxConsoleWidth, Boolean isEscCodesEnabled, String aBannerEscCode, String aBannerBorderEscCode, String aArgumentEscCode, String aCommandEscCode, String aOptionEscCode, String aDescriptionEscCode, String aLineSeparatorEscCode, String aResetEscCode, Consumer<Integer> aShutdownHook, PrintStream aStandardOut, PrintStream aErrorOut, ArgsParseMessageMode aArgsParseMessageMode, RuntimeLogger aLogger ) {
		return new AppHelper( aArgs, aShortOptionPrefix, aLongOptionPrefix, aArgsSyntax, aSyntaxMetrics, aTitle, aName, aDescription, aCopyright, aLicense, aExamples, aUrl, aInputStream, aFile, aFilePath, aConfigLocator, aResourceClass, aDelimiters, false, aPasswordPrompt, aEmptyArgsMessage, aBannerFont, aBannerFontPalette, aSeparatorLnChar, aTextBoxGrid, aLineBreak, aConsoleWidth, aMaxConsoleWidth, isEscCodesEnabled, aBannerEscCode, aBannerBorderEscCode, aArgumentEscCode, aCommandEscCode, aOptionEscCode, aDescriptionEscCode, aLineSeparatorEscCode, aResetEscCode, aShutdownHook, aStandardOut, aErrorOut, aArgsParseMessageMode, aLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@link Builder} to build {@link CtxAppHelper}.
	 */
	public static class Builder extends CtxHelper.Builder {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public CtxAppHelper build() {
			return new CtxAppHelper( this );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgsParseMessageMode( ArgsParseMessageMode aArgsParseMessageMode ) {
			super.withArgsParseMessageMode( aArgsParseMessageMode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAddExample( Example aExamples ) {
			super.withAddExample( aExamples );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAddExample( String aDescription, Operand<?>... aOperands ) {
			super.withAddExample( aDescription, aOperands );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( List<String> aArgs ) {
			super.withArgs( aArgs );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( List<String> aArgs, ArgsFilter aArgsFilter ) {
			return withArgs( aArgsFilter.toFiltered( aArgs ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( List<String> aArgs, Pattern aFilterExp ) {
			return withArgs( ArgsFilter.toFiltered( aArgs.toArray( new String[aArgs.size()] ), aFilterExp ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( String[] aArgs ) {
			super.withArgs( aArgs );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( String[] aArgs, ArgsFilter aArgsFilter ) {
			return withArgs( aArgsFilter.toFiltered( aArgs ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgs( String[] aArgs, Pattern aFilterExp ) {
			return withArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgsSyntax( Term aArgsSyntax ) {
			super.withArgsSyntax( aArgsSyntax );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgumentEscapeCode( String aArgumentEscCode ) {
			super.withArgumentEscapeCode( aArgumentEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBannerBorderEscapeCode( String aBannerBorderEscCode ) {
			super.withBannerBorderEscapeCode( aBannerBorderEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBannerEscapeCode( String aBannerEscCode ) {
			super.withBannerEscapeCode( aBannerEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBannerFont( Font aBannerFont ) {
			super.withBannerFont( aBannerFont );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBannerFontPalette( AsciiColorPalette aBannerFontPalette ) {
			super.withBannerFontPalette( aBannerFontPalette );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withBannerFontPalette( char[] aBannerFontPalette ) {
			super.withBannerFontPalette( aBannerFontPalette );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCommandEscapeCode( String aCommandEscCode ) {
			super.withCommandEscapeCode( aCommandEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withConfigLocator( ConfigLocator aConfigLocator ) {
			super.withConfigLocator( aConfigLocator );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withConsoleWidth( int aConsoleWidth ) {
			super.withConsoleWidth( aConsoleWidth );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withCopyright( String aCopyright ) {
			super.withCopyright( aCopyright );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withDescription( String aDescription ) {
			super.withDescription( aDescription );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withDescriptionEscapeCode( String aDescriptionEscCode ) {
			super.withDescriptionEscapeCode( aDescriptionEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEmptyArgsMessage( String aEmptyArgsMessage ) {
			super.withEmptyArgsMessage( aEmptyArgsMessage );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withErrorOut( PrintStream aErrorOut ) {
			super.withErrorOut( aErrorOut );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEscapeCodesEnabled( boolean aIsEscCodesEnabled ) {
			super.withEscapeCodesEnabled( aIsEscCodesEnabled );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withExamples( Collection<Example> aExamples ) {
			super.withExamples( aExamples );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withExamples( Example[] aExamples ) {
			super.withExamples( aExamples );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withFile( File aFile ) throws IOException, ParseException {
			return withFile( aFile, ConfigLocator.ALL );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withFile( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			super.withFile( aFile, aConfigLocator );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withFilePath( String aFilePath ) {
			super.withFilePath( aFilePath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withInputStream( InputStream aInputStream ) throws IOException, ParseException {
			super.withInputStream( aInputStream );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLicense( String aLicense ) {
			super.withLicense( aLicense );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLineBreak( String aLineBreak ) {
			super.withLineBreak( aLineBreak );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLineSeparatorEscapeCode( String aLineSeparatorEscCode ) {
			super.withLineSeparatorEscapeCode( aLineSeparatorEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLogger( RuntimeLogger aLogger ) {
			super.withLogger( aLogger );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLongOptionPrefix( String aLongOptionPrefix ) {
			super.withLongOptionPrefix( aLongOptionPrefix );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withMaxConsoleWidth( int aMaxConsoleWidth ) {
			super.withMaxConsoleWidth( aMaxConsoleWidth );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withName( String aName ) {
			super.withName( aName );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withOptionEscapeCode( String aOptionEscCode ) {
			super.withOptionEscapeCode( aOptionEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPasswordPrompt( String aPasswordPrompt ) {
			super.withPasswordPrompt( aPasswordPrompt );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResetEscapeCode( String aResetEscCode ) {
			super.withResetEscapeCode( aResetEscCode );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResourceClass( Class<?> aResourceClass ) {
			super.withResourceClass( aResourceClass );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResourceClass( Class<?> aResourceClass, String aFilePath ) throws IOException, ParseException {
			return withResourceClass( aResourceClass, aFilePath, ConfigLocator.ALL );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResourceClass( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			super.withResourceClass( aResourceClass, aFilePath, aConfigLocator );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResourceClass( String aFilePath ) throws IOException, ParseException {
			return withResourceClass( Execution.getMainClass(), aFilePath, ConfigLocator.ALL );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResourceClass( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
			return withResourceClass( null, aFilePath, aConfigLocator );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSeparatorLnChar( char aSeparatorLnChar ) {
			super.withSeparatorLnChar( aSeparatorLnChar );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withShortOptionPrefix( Character aShortOptionPrefix ) {
			super.withShortOptionPrefix( aShortOptionPrefix );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withShutDownHook( Consumer<Integer> aShutDownHook ) {
			super.withShutDownHook( aShutDownHook );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withStandardOut( PrintStream aStandardOut ) {
			super.withStandardOut( aStandardOut );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
			super.withSyntaxMetrics( aSyntaxMetrics );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
			return withSyntaxMetrics( (SyntaxMetrics) aSyntaxNotation );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
			super.withTextBoxGrid( aTextBoxGrid );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
			super.withTextBoxGrid( aTextBoxStyle );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withTitle( String aTitle ) {
			super.withTitle( aTitle );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withUrl( URL aUrl ) throws IOException, ParseException {
			super.withUrl( aUrl );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withVerboseFallback( boolean aVerboseFallback ) {
			super.withVerboseFallback( aVerboseFallback );
			return this;
		}
	}
}
