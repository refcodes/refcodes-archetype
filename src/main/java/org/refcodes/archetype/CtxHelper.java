// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.archetype;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.text.ParseException;
import java.util.Collection;
import java.util.function.Consumer;

import org.refcodes.archetype.CliHelper.ArgsParseMessageMode;
import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Example;
import org.refcodes.cli.Option;
import org.refcodes.cli.SyntaxMetrics;
import org.refcodes.cli.Term;
import org.refcodes.data.ExitCode;
import org.refcodes.decoupling.Dependency;
import org.refcodes.decoupling.ext.application.ApplicationReactor;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.properties.ProfileProperties;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.textual.Font;
import org.refcodes.textual.TextBoxGrid;
import org.refcodes.textual.TextBoxStyle;

/**
 * The {@link CtxHelper} enables harnessing the {@link ApplicationProperties}
 * alongside the {@link ApplicationProperties} profile support (as of
 * {@link ProfileProperties#getRuntimeProfiles()}) by automatically placing an
 * accordingly configured {@link ApplicationProperties} instance into the
 * container and enabling it for injection. The {@link ApplicationProperties}
 * {@link Dependency}'s alias is set to {@value #APPLICATION_PROPERTIES_ALIAS}
 * (as of this class's constant {@link #APPLICATION_PROPERTIES_ALIAS}).
 */
public class CtxHelper extends ApplicationReactor implements CliArchetype {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private CliHelper _cliHelper;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper() {}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( File aFile ) throws IOException, ParseException {
		super( aFile );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( File aFile, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( aFile, aConfigLocator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( File aFile, ConfigLocator aConfigLocator, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aFile, aConfigLocator, aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( File aFile, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aFile, aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( InputStream aInputStream ) throws IOException, ParseException {
		super( aInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( InputStream aInputStream, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aInputStream, aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( String aFilePath ) throws IOException, ParseException {
		super( aFilePath );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		super( aFilePath, aConfigLocator );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( String aFilePath, ConfigLocator aConfigLocator, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aFilePath, aConfigLocator, aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( String aFilePath, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aFilePath, aArgs );
	}

	/**
	 * Creates a new {@link CtxHelper} with all the properties mainly regarded
	 * to the {@link ApplicationProperties} used under the hood (for ease of use
	 * please use the {@link AbstractCliBuilder} be calling the
	 * {@link #builder()} method).
	 *
	 * @param aArgs The command line arguments.
	 * @param aShortOptionPrefix The short options' prefix to be set.
	 * @param aLongOptionPrefix The long options' prefix to be set.
	 * @param aArgsSyntax The args syntax root node of your command line syntax.
	 * @param aSyntaxMetrics the syntax metrics
	 * @param aTitle The title of the application.
	 * @param aName The name of your application.
	 * @param aDescription The description to use when printing out the help.
	 * @param aCopyright The copyright being applied.
	 * @param aLicense The license for the application.
	 * @param aExamples The {@link Example} examples to use when printing out
	 *        the help.
	 * @param aUrl The {@link URL} from which to load the configuration.
	 * @param aInputStream The {@link InputStream} from which to load the
	 *        configuration.s
	 * @param aFile {@link File} from which to load the configuration.
	 * @param aFilePath The name to your default config file.
	 * @param aConfigLocator The {@link ConfigLocator} to use when locating the
	 *        configuration file (defaults to {@link ConfigLocator#DEFAULT}.
	 * @param aResourceClass The application's {@link Class} which to use when
	 *        loading resources (of the according module) by invoking
	 *        {@link Class#getResourceAsStream(String)}.
	 * @param aDelimiters The delimiters to be used when parsing specific
	 *        configuration file notations.
	 * @param aIsVerboseFallback the is verbose fallback
	 * @param aPasswordPrompt The prompt to use when requesting a password.
	 * @param aEmptyArgsMessage The message to show when args parsing failed and
	 *        the args have been empty.
	 * @param aBannerFont The font for the banner to use.
	 * @param aBannerFontPalette The ASCII color palette for the banner to use.
	 * @param aSeparatorLnChar The line separator char to be used.
	 * @param aTextBoxGrid The {@link TextBoxGrid} (or {@link TextBoxStyle}) to
	 *        be used when drawing the banner.
	 * @param aLineBreak The line break to use when printing to the terminal.
	 * @param aConsoleWidth The console width when printing to the terminal.
	 * @param aMaxConsoleWidth The max console width when printing to the
	 *        terminal.
	 * @param isEscCodesEnabled True in case the usage of Escape-Codes is
	 *        enabled.
	 * @param aBannerEscCode The banner Escape-Code to use when printing to the
	 *        terminal.
	 * @param aBannerBorderEscCode The banner border Escape-Code to use when
	 *        printing to the terminal.
	 * @param aArgumentEscCode The argument Escape-Code to use when printing to
	 *        the terminal.
	 * @param aCommandEscCode The command Escape-Code to use when printing to
	 *        the terminal.
	 * @param aOptionEscCode The option Escape-Code to use when printing to the
	 *        terminal.
	 * @param aDescriptionEscCode The description Escape-Code to use when
	 *        printing to the terminal.
	 * @param aLineSeparatorEscCode The line separator Escape-Code to use when
	 *        printing to the terminal.
	 * @param aResetEscCode The reset Escape-Code to use when printing to the
	 *        terminal.
	 * @param aShutdownHook the shutdown hook
	 * @param aStandardOut The {@link PrintStream} to use for standard out.
	 * @param aErrorOut The {@link PrintStream} to use for error out.
	 * @param aArgsParseMessageMode The {@link ArgsParseMessageMode} to use when
	 *        displaying error messages as of failure when parsing command line
	 *        arguments.
	 * @param aLogger The application's logger.
	 */
	public CtxHelper( String[] aArgs, Character aShortOptionPrefix, String aLongOptionPrefix, Term aArgsSyntax, SyntaxMetrics aSyntaxMetrics, String aTitle, String aName, String aDescription, String aCopyright, String aLicense, Collection<Example> aExamples, URL aUrl, InputStream aInputStream, File aFile, String aFilePath, ConfigLocator aConfigLocator, Class<?> aResourceClass, char[] aDelimiters, boolean aIsVerboseFallback, String aPasswordPrompt, String aEmptyArgsMessage, Font aBannerFont, char[] aBannerFontPalette, Character aSeparatorLnChar, TextBoxGrid aTextBoxGrid, String aLineBreak, Integer aConsoleWidth, Integer aMaxConsoleWidth, Boolean isEscCodesEnabled, String aBannerEscCode, String aBannerBorderEscCode, String aArgumentEscCode, String aCommandEscCode, String aOptionEscCode, String aDescriptionEscCode, String aLineSeparatorEscCode, String aResetEscCode, Consumer<Integer> aShutdownHook, PrintStream aStandardOut, PrintStream aErrorOut, ArgsParseMessageMode aArgsParseMessageMode, RuntimeLogger aLogger ) {
		_cliHelper = toCliHelper( aArgs, aShortOptionPrefix, aLongOptionPrefix, aArgsSyntax, aSyntaxMetrics, aTitle, aName, aDescription, aCopyright, aLicense, aExamples, aUrl, aInputStream, aFile, aFilePath, aConfigLocator, aResourceClass, aDelimiters, aPasswordPrompt, aEmptyArgsMessage, aBannerFont, aBannerFontPalette, aSeparatorLnChar, aTextBoxGrid, aLineBreak, aConsoleWidth, aMaxConsoleWidth, isEscCodesEnabled, aBannerEscCode, aBannerBorderEscCode, aArgumentEscCode, aCommandEscCode, aOptionEscCode, aDescriptionEscCode, aLineSeparatorEscCode, aResetEscCode, aShutdownHook, aStandardOut, aErrorOut, aArgsParseMessageMode, aLogger );
		_properties = _cliHelper.getApplicationProperties();
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( URL aUrl ) throws IOException, ParseException {
		super( aUrl );
	}

	/**
	 * {@inheritDoc}
	 */
	public CtxHelper( URL aUrl, String[] aArgs ) throws IOException, ParseException, ArgsSyntaxException {
		super( aUrl, aArgs );
	}

	protected CtxHelper( AbstractCliBuilder<?, ?> aBuilder ) {
		this( aBuilder.args, aBuilder.shortOptionPrefix, aBuilder.longOptionPrefix, aBuilder.argsSyntax, aBuilder.syntaxMetrics, aBuilder.title, aBuilder.name, aBuilder.description, aBuilder.copyright, aBuilder.license, aBuilder.examples, aBuilder.url, aBuilder.inputStream, aBuilder.file, aBuilder.filePath, aBuilder.configLocator, aBuilder.resourceClass, aBuilder.delimiters, aBuilder.verboseFallback, aBuilder.passwordPrompt, aBuilder.emptyArgsMessage, aBuilder.bannerFont, aBuilder.bannerFontPalette, aBuilder.separatorLnChar, aBuilder.textBoxGrid, aBuilder.lineBreak, aBuilder.consoleWidth, aBuilder.maxConsoleWidth, aBuilder.escCodesEnabled, aBuilder.bannerEscCode, aBuilder.bannerBorderEscCode, aBuilder.argumentEscCode, aBuilder.commandEscCode, aBuilder.optionEscCode, aBuilder.descriptionEscCode, aBuilder.lineSeparatorEscCode, aBuilder.resetEscCode, aBuilder.shutDownHook, aBuilder.standardOut, aBuilder.errorOut, aBuilder.argsParseMessageMode, aBuilder.logger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createResourceFile( String aResourcePath, File aDestinationFile ) throws IOException {
		_cliHelper.createResourceFile( aResourcePath, aDestinationFile );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createResourceFile( String aResourcePath, String aDestinationPath ) throws IOException {
		_cliHelper.createResourceFile( aResourcePath, aDestinationPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exit( ExitCode aExitCode ) {
		_cliHelper.exit( aExitCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exit( int aStatusCode ) {
		_cliHelper.exit( aStatusCode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exitOnException( Throwable e ) {
		_cliHelper.exitOnException( e );
	}

	/**
	 * Gets the runtime properties.
	 *
	 * @return the runtime properties
	 * 
	 * @see org.refcodes.archetype.CliHelper#getApplicationProperties()
	 */
	@Override
	public ApplicationProperties getApplicationProperties() {
		return _cliHelper.getApplicationProperties();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDebug() {
		return _cliHelper.isDebug();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVerbose() {
		return _cliHelper.isVerbose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printException( Throwable aE ) {
		_cliHelper.printException( aE );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSysInfo() {
		_cliHelper.printSysInfo();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] readPassword() {
		return _cliHelper.readPassword();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] readPassword( String aPrompt ) {
		return _cliHelper.readPassword( aPrompt );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toOptions( Option<?> aOption ) {
		return _cliHelper.toOptions( aOption );
	}

	/**
	 * Creates builder to build {@link CtxHelper}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected CliHelper toCliHelper( String[] aArgs, Character aShortOptionPrefix, String aLongOptionPrefix, Term aArgsSyntax, SyntaxMetrics aSyntaxMetrics, String aTitle, String aName, String aDescription, String aCopyright, String aLicense, Collection<Example> aExamples, URL aUrl, InputStream aInputStream, File aFile, String aFilePath, ConfigLocator aConfigLocator, Class<?> aResourceClass, char[] aDelimiters, String aPasswordPrompt, String aEmptyArgsMessage, Font aBannerFont, char[] aBannerFontPalette, Character aSeparatorLnChar, TextBoxGrid aTextBoxGrid, String aLineBreak, Integer aConsoleWidth, Integer aMaxConsoleWidth, Boolean isEscCodesEnabled, String aBannerEscCode, String aBannerBorderEscCode, String aArgumentEscCode, String aCommandEscCode, String aOptionEscCode, String aDescriptionEscCode, String aLineSeparatorEscCode, String aResetEscCode, Consumer<Integer> aShutdownHook, PrintStream aStandardOut, PrintStream aErrorOut, ArgsParseMessageMode aArgsParseMessageMode, RuntimeLogger aLogger ) {
		return new CliHelper( aArgs, aShortOptionPrefix, aLongOptionPrefix, aArgsSyntax, aSyntaxMetrics, aTitle, aName, aDescription, aCopyright, aLicense, aExamples, aUrl, aInputStream, aFile, aFilePath, aConfigLocator, aResourceClass, aDelimiters, false, aPasswordPrompt, aEmptyArgsMessage, aBannerFont, aBannerFontPalette, aSeparatorLnChar, aTextBoxGrid, aLineBreak, aConsoleWidth, aMaxConsoleWidth, isEscCodesEnabled, aBannerEscCode, aBannerBorderEscCode, aArgumentEscCode, aCommandEscCode, aOptionEscCode, aDescriptionEscCode, aLineSeparatorEscCode, aResetEscCode, aShutdownHook, aStandardOut, aErrorOut, aArgsParseMessageMode, aLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@link Builder} to build {@link CliHelper}.
	 */
	public static class Builder extends AbstractCliBuilder<Builder, CtxHelper> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public CtxHelper build() {
			return new CtxHelper( this );
		}
	}
}
